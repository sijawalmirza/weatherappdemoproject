import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';


@Injectable()
export class WeatherService {
  constructor(
    private http: HttpClient) {
  }

  getWeatherForecast(coordinates): Observable<any> {
    return this.http.get('https://api.openweathermap.org/data/2.5/onecall?lat=' + coordinates.lat + '&lon=' + coordinates.lon + '&exclude=minutely&appid=7245a57c4f4bcefd251eda50f036b70a');
  }

  getCityByCoordinates(coordinates): Observable<any> {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + coordinates.lat + ',' + coordinates.lon + '&sensor=true&key=AIzaSyDNEt4zO7mwGEl98ZMNYKzPm6D9B4tJEtE'
    );
  }
}
