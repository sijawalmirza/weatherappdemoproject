import {Component, ViewChild} from '@angular/core';
import {WeatherService} from './services/weather.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import {GooglePlaceDirective} from 'ngx-google-places-autocomplete';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class WeatherForecastComponent {

  constructor(private weatherForecastService: WeatherService) {
  }

  cityName: string;
  @ViewChild('placesRef') placesRef: GooglePlaceDirective;
  placesFormControl: FormControl = new FormControl('');
  options = {
    types: ['(cities)'],
    componentRestrictions: {country: 'US'}
  };
  toCelsiusConstant: number = 273.15;
  currentDate: number = Date.now();
  weatherTableDataSource: WeatherForecastModel[];
  weatherForecastTableColumns: string[] = ['Day', 'TempDay', 'TempNight', 'Feels like day', 'Feels like night', 'Clouds'];
  weatherForecastTableExpandedElement: WeatherForecastModel | null;
  currentWeatherForecast: { temp: number, feels_like: number, weather: any };

  date = new Date();

// Getting location from angular navigator
  getCurrentLocation(): void {
    navigator.geolocation.getCurrentPosition(res => {
      // Getting weather information from weather service
      this.getForecast({lat: res.coords.latitude, lon: res.coords.longitude});
      // Getting city from google's geolocation API
      this.weatherForecastService.getCityByCoordinates({lat: res.coords.latitude, lon: res.coords.longitude}).subscribe(res => {
        this.cityName = res.results[0].address_components[0].short_name;
        this.placesFormControl.reset();
      });
    }, err => {
    });
  }

  // extracted the function in order to reuse it
  // this function calls the weather api to get the weather forcast of current and next 5 days
  private getForecast(coordinates): void {
    this.date = new Date();
    this.weatherForecastService.getWeatherForecast(coordinates)
      .subscribe(forecast => {
        // Adding date as api is not providing date
        for (let i = 1; i <= forecast.daily.length; i++) {
          forecast.daily[i - 1].day = this.date.setDate(this.date.getDate() + 1);
        }

        this.weatherTableDataSource = forecast.daily.slice(0, 5);
        this.currentWeatherForecast = forecast.current;
      });
  }

  // This event is triggered when ever a location is selected from the place autocomplete field
  public handleAddressChange(address: Address): void {
    console.log(address);
    this.cityName = address.address_components[0].short_name;
    this.getForecast({lat: address.geometry.location.lat(), lon: address.geometry.location.lng()});

  }
}

// Material table elements interface
export interface WeatherForecastModel {
  Day: string;
  position: number;
  Temperature: number;
  Forecast: string;
  temp: any;
  feels_like: any;
  weather: any;
}
