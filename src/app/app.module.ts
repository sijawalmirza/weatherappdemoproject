import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {WeatherForecastComponent} from './weather-forecast.component';
import {WeatherService} from './services/weather.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import { MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    WeatherForecastComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, BrowserAnimationsModule, MatCardModule, MatButtonModule, MatTableModule, GooglePlaceModule, MatInputModule, ReactiveFormsModule
  ],
  providers: [WeatherService],
  bootstrap: [WeatherForecastComponent]
})
export class AppModule {
}
